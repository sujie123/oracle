# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

1. 以hr用户登录。
2. 创建一个包(Package)，包名是MyPack。
3. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
4. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。

递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 实验步骤

1. 创建包MyPack:

```sql
CREATE OR REPLACE PACKAGE MyPack IS
  FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
  PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
```

1. 实现MyPack包:

```sql
CREATE OR REPLACE PACKAGE BODY MyPack IS
  FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER AS
    N NUMBER(20,2);
  BEGIN
    SELECT SUM(salary) INTO N FROM EMPLOYEES E WHERE E.DEPARTMENT_ID = V_DEPARTMENT_ID;
    RETURN N;
  END;

  PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER) AS
    LEFTSPACE VARCHAR(2000);
  BEGIN
    LEFTSPACE := ' ';
    FOR v IN (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
              START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
              CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
      DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
  END;
END MyPack;
/
```

1. 测试函数Get_SalaryAmount:

```sql
SELECT department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total FROM departments;
```

1. 测试过程Get_Employees:

```sql
SET SERVEROUTPUT ON;
DECLARE
  V_EMPLOYEE_ID NUMBER;    
BEGIN
  V_EMPLOYEE_ID := 101;
  MYPACK.Get_Employees(V_EMPLOYEE_ID => V_EMPLOYEE_ID);    
END;
/
```

## 实验结论

在本实验中，我们使用PL/SQL语言创建了一个包MyPack，包中包含了一个函数和一个过程。通过创建和使用这些函数和过程，我们掌握了Oracle PL/SQL语言的基本结构和语法，以及如何声明和使用变量和常量，以及如何使用包，过程和函数。