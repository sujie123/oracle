## 作业要求：

在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。



## 实验步骤：

1. 以system用户身份登录到pdborcl数据库：

```sql
sqlplus system/密码@pdborcl
```



2. 创建角色con_res_role并授予connect、resource和CREATE VIEW权限：

```sql
CREATE ROLE con_res_role;
GRANT connect, resource, CREATE VIEW TO con_res_role;
```



3. 创建用户sale并设置默认表空间为users，且限额为50M，授予con_res_role角色：

```sql
CREATE USER sale IDENTIFIED BY 密码 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sale DEFAULT TABLESPACE "USERS";
ALTER USER sale QUOTA 50M ON users;
GRANT con_res_role TO sale;
```



4. 测试sale用户连接数据库并创建表、插入数据、创建视图和查询表和视图的数据：

```sql
--使用sale用户登录
sqlplus sale/密码@pdborcl

--创建表
CREATE TABLE customers (id NUMBER, name VARCHAR2(50)) TABLESPACE "USERS";

--插入数据
INSERT INTO customers (id, name) VALUES (1, 'zhang');
INSERT INTO customers (id, name) VALUES (2, 'wang');

--创建视图
CREATE VIEW customers_view AS SELECT name FROM customers;

--给hr用户共享视图customers_view
GRANT SELECT ON customers_view TO hr;

--查询视图的数据
SELECT * FROM customers_view;

--退出用户sale登录
exit;
```



5. 使用hr用户登录数据库并查询sale授权的视图：

```sql
--使用hr用户登录
sqlplus hr/密码@pdborcl

--查询视图的数据
SELECT * FROM sale.customers_view;

--退出用户hr登录
exit;
```



6. 设置概要文件限制sale用户最多登录3次错误密码后被锁定：

```sql
--使用system用户登录
sqlplus system/密码@pdborcl

--设置概要文件
ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```



7. 测试用户锁定和解锁：

```sql
--使用sale用户登录并输入错误密码3次，用户被锁定
sqlplus sale/错误密码@pdborcl

--使用system用户登录解锁sale用户
sqlplus system/密码@pdborcl
ALTER USER sale ACCOUNT UNLOCK;
```



8. 查询表空间和数据库使用情况：

```sql
-- 查询表空间的数据文件及其磁盘使用情况
SELECT tablespace_name, file_name, bytes/1024/1024 MB, maxbytes/1024/1024 MAX_MB, autoextensible
FROM dba_data_files
WHERE tablespace_name = 'USERS';

-- 查询表空间的使用情况
SELECT a.tablespace_name "表空间名", Total/1024/1024 "大小MB",
       free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
       Round(( total - free )/ total,4)* 100 "使用率%"
FROM (SELECT tablespace_name, SUM(bytes)free
      FROM   dba_free_space
      GROUP BY tablespace_name)a,
     (SELECT tablespace_name, SUM(bytes)total
      FROM dba_data_files
      GROUP BY tablespace_name)b
WHERE  a.tablespace_name = b.tablespace_name;

-- 查询数据库文件的使用情况
SELECT tablespace_name, SUM(bytes)/1024/1024 "已使用空间（MB）", SUM(maxbytes)/1024/1024 "最大空间（MB）", SUM(bytes)/SUM(maxbytes) "使用率"
FROM dba_data_files
GROUP BY tablespace_name;
```



9. 删除用户和角色：

```sql
--使用system用户登录删除用户和角色
sqlplus system/密码@pdborcl
DROP USER sale CASCADE;
DROP ROLE con_res_role;
```



### 总结

本次实验主要涉及到Oracle数据库的用户和权限管理。在这个实验中，我们学习了如何创建本地角色、创建用户、授权角色、设置表空间的限额以及测试用户权限等。同时，我们还学习了如何查询表空间和数据库文件的使用情况。

总的来说，这个实验可以帮助我们更好地了解Oracle数据库的用户和权限管理方面的知识，并且让我们掌握了一些基本的数据库管理操作。在未来的实际工作中，这些知识和技能都将是非常有用的。

通过本次实验，我们应该掌握了以下内容：

1. 创建本地角色，并授权角色相关的权限；
2. 创建用户，并将角色授权给用户；
3. 设置表空间的限额；
4. 测试用户权限，包括创建表、插入数据、创建视图和查询表和视图的数据等；
5. 查询表空间和数据库文件的使用情况。

总之，这个实验对我们理解和掌握Oracle数据库的用户和权限管理方面的知识是非常有帮助的。通过这个实验，我们可以更好地了解数据库的管理和操作，并在今后的工作中更加熟练地使用这些知识和技能。