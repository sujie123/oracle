# 实验4：PL/SQL语言打印杨辉三角



## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。



## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。



## 实验步骤：

### 1. 运行杨辉三角源代码，熟悉其功能。

源代码如下：

```sql
DECLARE
  TYPE t_number IS VARRAY(100) OF INTEGER NOT NULL;
  i INTEGER;
  j INTEGER;
  spaces VARCHAR2(30) := '   ';
  rowArray t_number := t_number();
BEGIN
  DBMS_OUTPUT.PUT_LINE('1');
  DBMS_OUTPUT.PUT(RPAD(1, 9, ' '));
  DBMS_OUTPUT.PUT(RPAD(1, 9, ' '));
  DBMS_OUTPUT.PUT_LINE('');

  FOR i IN 1 .. 9 LOOP
    rowArray.EXTEND;
  END LOOP;
  rowArray(1) := 1;
  rowArray(2) := 1;

  FOR i IN 3 .. 9 LOOP
    rowArray(i) := 1;
    j := i - 1;

    WHILE j > 1 LOOP
      rowArray(j) := rowArray(j) + rowArray(j - 1);
      j := j - 1;
    END LOOP;

    FOR j IN 1 .. i LOOP
      DBMS_OUTPUT.PUT(RPAD(rowArray(j), 9, ' '));
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('');
  END LOOP;
END;
```

### 2. 创建存储过程YHTriangle。

```sql
CREATE OR REPLACE PROCEDURE YHTriangle (N IN INTEGER)
AS
  TYPE t_number IS VARRAY(100) OF INTEGER NOT NULL;
  i INTEGER;
  j INTEGER;
  spaces VARCHAR2(30) := '   ';
  rowArray t_number := t_number();
BEGIN
  DBMS_OUTPUT.PUT_LINE('1');
  DBMS_OUTPUT.PUT(RPAD(1, 9, ' '));
  DBMS_OUTPUT.PUT(RPAD(1, 9, ' '));
  DBMS_OUTPUT.PUT_LINE('');

  FOR i IN 1 .. N LOOP
    rowArray.EXTEND;
  END LOOP;
  rowArray(1) := 1;
  rowArray(2) := 1;

  FOR i IN 3 .. N LOOP
    rowArray(i) := 1;
    j := i - 1;

    WHILE j > 1 LOOP
      rowArray(j) := rowArray(j) + rowArray(j - 1);
      j := j - 1;
    END LOOP;

    FOR j IN 1 .. i LOOP
      DBMS_OUTPUT.PUT(RPAD(rowArray(j), 9, ' '));
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('');
  END LOOP;
END YHTriangle;
/
```

### 3. 编译存储过程YHTriangle。

```sql
ALTER PROCEDURE YHTriangle COMPILE;
```

### 4. 运行存储过程YHTriangle，打印N行杨辉三角。

```sql
SET SERVEROUTPUT ON;
EXEC YHTriangle(9);
```



## 实验结论

通过本次实验，我们学习了Oracle PL/SQL语言的基本语法和编写存储过程的方法。在实验中，我们将杨辉三角源代码转换为存储过程YHTriangle，并通过传入不同的参数N，打印出相应行数的杨辉三角。存储过程的使用，可以将常用的功能封装成一个可重复使用的模块，方便数据库管理员和开发人员在实际工作中使用。