CREATE OR REPLACE PACKAGE ecommerce_package AS 
    PROCEDURE create_order (p_user_id IN Orders.user_id%TYPE);
    PROCEDURE add_order_detail (p_order_id IN OrderDetails.order_id%TYPE, p_product_id IN OrderDetails.product_id%TYPE, p_quantity IN OrderDetails.quantity%TYPE);
    FUNCTION calculate_total_price (p_order_id IN OrderDetails.order_id%TYPE) RETURN NUMBER;
END ecommerce_package;

CREATE OR REPLACE PACKAGE BODY ecommerce_package AS 
    PROCEDURE create_order (p_user_id IN Orders.user_id%TYPE) IS
    BEGIN
        INSERT INTO Orders (order_id, user_id, order_date) VALUES (Orders_seq.NEXTVAL, p_user_id, SYSDATE);
    END create_order;

    PROCEDURE add_order_detail (p_order_id IN OrderDetails.order_id%TYPE, p_product_id IN OrderDetails.product_id%TYPE, p_quantity IN OrderDetails.quantity%TYPE) IS
    BEGIN
        INSERT INTO OrderDetails (order_id, product_id, quantity) VALUES (p_order_id, p_product_id, p_quantity);
    END add_order_detail;

    FUNCTION calculate_total_price (p_order_id IN OrderDetails.order_id%TYPE) RETURN NUMBER IS
        v_total_price NUMBER(10,2);
    BEGIN
        SELECT SUM(quantity * price) INTO v_total_price
        FROM OrderDetails JOIN Products ON OrderDetails.product_id = Products.product_id
        WHERE order_id = p_order_id;
        RETURN v_total_price;
    END calculate_total_price;
END ecommerce_package;
/
