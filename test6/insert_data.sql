BEGIN
   FOR i IN 1..100000 LOOP
      INSERT INTO Users (user_id, username, password) 
      VALUES (i, 'User' || i, DBMS_RANDOM.string('a', 10));

      INSERT INTO Products (product_id, product_name, price) 
      VALUES (i, 'Product' || i, DBMS_RANDOM.value(1,100));
   END LOOP;
   
   FOR i IN 1..100000 LOOP
      INSERT INTO Orders (order_id, user_id, order_date) 
      VALUES (i, i, SYSDATE - DBMS_RANDOM.value(1,365));
      
      INSERT INTO OrderDetails (order_id, product_id, quantity) 
      VALUES (i, DBMS_RANDOM.value(1,100000), DBMS_RANDOM.value(1,10));
   END LOOP;
   COMMIT;
END;
/
