CREATE TABLESPACE tablespace_a
DATAFILE 'tablespace_a.dat'
SIZE 10M AUTOEXTEND ON;

CREATE TABLESPACE tablespace_b
DATAFILE 'tablespace_b.dat'
SIZE 10M AUTOEXTEND ON;

CREATE TABLE Users (
    user_id NUMBER,
    username VARCHAR2(50),
    password VARCHAR2(50)
) TABLESPACE tablespace_a;

CREATE TABLE Products (
    product_id NUMBER,
    product_name VARCHAR2(50),
    price DECIMAL(10, 2)
) TABLESPACE tablespace_a;

CREATE TABLE Orders (
    order_id NUMBER,
    user_id NUMBER,
    order_date DATE
) TABLESPACE tablespace_b;

CREATE TABLE OrderDetails (
    order_id NUMBER,
    product_id NUMBER,
    quantity NUMBER
) TABLESPACE tablespace_b;
