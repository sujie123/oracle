# 基于Oracle数据库的商品销售系统设计

## 数据库设计

### 表空间

在Oracle数据库中，创建了两个表空间：tablespace_a和tablespace_b，以支持分离和优化存储。并在其中分别创建了四个表。相关的SQL操作在 "create_tables.sql" 文件中。

- tablespace_a: 存储Users和Products表。
- tablespace_b: 存储Orders和OrderDetails表。



### 数据表

设计并创建了四个表：

1. Users表：存储用户信息，包括用户ID，用户名，密码等。用户ID设为主键，用户名设为唯一键，避免重复。

2. Products表：存储商品信息，包括商品ID，商品名称，价格等。商品ID设为主键。

3. Orders表：存储订单信息，包括订单ID，下单用户，下单时间等。订单ID设为主键，下单用户与Users表的用户ID相关联。

4. OrderDetails表：存储订单详细信息，包括订单ID，商品ID，购买数量等。订单ID与商品ID共同作为复合主键，订单ID与Orders表的订单ID相关联，商品ID与Products表的商品ID相关联。



### 模拟数据

利用PL/SQL脚本生成了10万条模拟数据，并通过批量插入的方式将这些数据插入到四个表中。

在Oracle中生成随机数据可以使用DBMS_RANDOM函数。以下插入了10万条用户数据和商品数据。对于订单和订单详情数据，我们假设每个用户至少有一个订单，每个订单至少有一个商品。

相关的SQL操作在 "insert_data.sql" 文件中。



## 权限及用户分配

相关的SQL操作在 "create_users.sql" 文件中。

### 用户创建

在Oracle数据库中，创建了两个用户：

- Admin：拥有数据库的全部管理权限。
- Shopper：普通购物用户，只拥有查询商品、创建订单、查看订单等权限。

### 权限分配

对用户的权限进行了精细划分：

- Admin：赋予了对所有表的增删改查权限。
- Shopper：赋予了对Orders和OrderDetails表的增查权限，对Products表的查权限。



## 存储过程和函数

创建了名为ecommerce_package的程序包，其中包含了三个处理业务逻辑的存储过程和函数：

相关的SQL操作在 "create_procedures_and_functions.sql" 文件中。

- create_order：根据用户ID创建订单，订单创建成功后，返回新订单的订单ID。

- add_order_detail：根据订单ID、商品ID和购买数量向订单中添加商品。

- calculate_total_price：根据订单ID计算订单的总价，并返回订单总价。



## 数据库备份方案

设计了一个基于RMAN的数据库备份策略。创建了备份脚本，可按需调整备份的频率和目标路径，以确保数据库的安全性和数据的完整性。相关的SQL操作在 "backup_database.sql" 文件中。