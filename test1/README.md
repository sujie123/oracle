# SQL语句的执行计划分析与优化指导实验

## 实验目的

本实验的目的是分析SQL执行计划，执行SQL语句的优化指导，理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

实验数据库是pdborcl，用户是sys和hr。

## 实验内容

本实验对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。首先，我们将使用以下语句授予用户hr以下视图的选择权限：

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/喷脸ustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr;
```

接下来，我们将设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后，我们将使用sqldeveloper的优化指导工具对我们认为最优的SQL语句进行优化指导，看看该工具有没有给出优化建议。

教材中的查询语句为：查询两个部门('IT'和'Sales')的部门总人数和平均工资，两个查询的结果是一样的。但效率不相同。

查询1：

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba

set autotrace on

SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
from hr.departments d,hr.employees e
where d.department_id = e.department_id
and d.department_name in ('IT','Sales')
GROUP BY d.department_name;
```

查询2：

```sql
set autotrace on

SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
FROM hr.departments d,hr.employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
HAVING d.department_name in ('IT','Sales');
```

## 实验分析

我们分别执行了两个查询语句，并使用autotrace on命令获取了它们的执行计划和统计信息。我们可以看到，查询2的执行计划比查询1更优。

查询1的执行计划如下：

```sql
Predicate Information (identified by operation id):
---------------------------------------------------
   4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
Note
-----
   - this is an adaptive plan
```

查询2的执行计划如下：

```sql
Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   6 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
       filter("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
```

我们可以看到，查询2的执行计划比查询1更简单，并且没有使用自适应计划。查询2的统计信息也比查询1更好，它的consistent gets比查询1少了一个，而且只进行了一个排序（memory），而查询1没有进行排序。

## 结果文档说明书

我们认为查询2是最优的，因为它的执行计划更简单，统计信息也更好。我们使用sqldeveloper的优化指导工具对查询2进行了优化指导，但该工具并没有给出任何优化建议。这可能是因为查询2已经是最优的了。

在本实验中，我们学习了如何分析SQL执行计划，以及如何使用sqldeveloper的优化指导工具进行优化指导。我们还学习了如何授予用户hr以下视图的选择权限，以便进行实验。
