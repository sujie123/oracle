## 实验目的

- 掌握分区表的创建方法，掌握各种分区方式的使用场景。



## 实验内容

1. 创建两张表：订单表(orders)与订单详表(order_details)。
2. 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
3. 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
4. orders表按订单日期（order_date）设置范围分区。
5. order_details表设置引用分区。
6. 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
7. 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
8. 进行分区与不分区的对比实验。



## 实验步骤

### 1：创建orders表

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);


--以后再逐年增加新年份的分区
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;
```

### 2：创建order_details表

```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```

### 3：创建两个序列

```sql
CREATE SEQUENCE SEQ1 MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE NOKEEP NOSCALE GLOBAL;
CREATE SEQUENCE SEQ2 MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE NOKEEP NOSCALE GLOBAL;
```

### 4：插入数据

```sql
-- 插入orders表数据
DECLARE
  i INTEGER;
  y INTEGER;
  m INTEGER;
  d INTEGER;
  str VARCHAR2(100);
  random_name VARCHAR2(40);
  random_tel VARCHAR2(40);
  random_employee_id INTEGER;
BEGIN
  i := 0;
  y := 2015;
  m := 1;
  d := 12;
  WHILE i < 100 LOOP
    i := i + 1;
    m := m + 1;
    IF m > 12 THEN
      m := 1;
    END IF;
    str := y || '-' || m || '-' || d;
    random_name := 'Customer_' || i;
    random_tel := 'Tel_' || i;
    random_employee_id := TRUNC(DBMS_RANDOM.VALUE(1, 1000));
    INSERT INTO orders (order_id, customer_name, customer_tel, order_date, employee_id)
      VALUES (SEQ1.NEXTVAL, random_name, random_tel, TO_DATE(str, 'yyyy-MM-dd'), random_employee_id);
  END LOOP;
  COMMIT;
END;
/

-- 插入order_details表数据
DECLARE
  i INTEGER;
  j INTEGER;
  order_id_val INTEGER;
  product_id_val VARCHAR2(40);
  product_num_val NUMBER(8, 2);
  product_price_val NUMBER(8, 2);
BEGIN
  i := 0;
  WHILE i < 100 LOOP
    i := i + 1;
    SELECT order_id INTO order_id_val FROM (SELECT order_id, ROWNUM AS rn FROM orders) WHERE rn = i;
    j := 0;
    WHILE j < 5 LOOP
      j := j + 1;
      product_id_val := 'Product_' || (i * 5 + j);
      product_num_val := TRUNC(DBMS_RANDOM.VALUE(1, 10));
      product_price_val := TRUNC(DBMS_RANDOM.VALUE(100, 1000), 2);
      INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
        VALUES (SEQ1.NEXTVAL, order_id_val, product_id_val, product_num_val, product_price_val);
    END LOOP;
  END LOOP;
  COMMIT;
END;
/
```

### 5：查询数据

```sql
SELECT * FROM orders;
SELECT * FROM order_details;
```

### 6：联合查询

```sql
SELECT o.order_id, o.customer_name, o.customer_tel, o.order_date, od.product_id, od.product_num, od.product_price
FROM orders o
JOIN order_details od ON o.order_id = od.order_id;
```

### 7：分析执行计划

```sql
EXPLAIN PLAN FOR
SELECT o.order_id, o.customer_name, o.customer_tel, o.order_date, od.product_id, od.product_num, od.product_price
FROM orders o
JOIN order_details od ON o.order_id = od.order_id;
SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY);
```



## 实验结论

通过本次实验，我们了解了分区表的创建方法和各种分区方式的使用场景。我们成功创建了两张表并进行了数据插入和联合查询，同时进行了分区与不分区的对比实验。

根据实验结果，我们可以得出以下结论：

1. 分区表的查询性能通常优于非分区表，因为数据库可以根据查询条件仅访问与分区键值相关的分区，从而减少I/O操作和数据扫描量。
2. 范围分区适用于具有连续值域的列，如时间日期。通过使用范围分区，可以根据数据的分布特征将数据存储在不同的分区上，提高查询性能。
3. 引用分区适用于具有主外键关联的表，能够确保与主表相同的分区键值存储在相同的分区上。这样，在进行联合查询时，数据库可以在相同的分区上处理数据，从而提高查询性能。

综上所述，分区表能够提高查询性能，特别是在处理大量数据的情况下。根据数据的分布特征和查询需求，选择合适的分区方式是实现高性能数据库设计的关键。

在今后的数据库设计和维护中，我们将更加深入地了解分区表的使用，并结合具体的业务需求，选择合适的分区方式来优化查询性能。